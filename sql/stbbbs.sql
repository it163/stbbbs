/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : stbbbs

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2018-04-15 21:56:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `var` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '栏目英文常量名称，不可变',
  `title` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '栏目的标题',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态，布尔值',
  `sort` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('2', 'jobs', '招聘', '1', '1');
INSERT INTO `category` VALUES ('3', 'hunt', '头条', '1', '2');
INSERT INTO `category` VALUES ('4', 'qa', '问答', '1', '3');

-- ----------------------------
-- Table structure for topics
-- ----------------------------
DROP TABLE IF EXISTS `topics`;
CREATE TABLE `topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'qa' COMMENT '所属栏目',
  `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文章标题',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文章内容',
  `is_down` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否下沉帖子，下沉的帖子不在话题列表显示',
  `like_num` int(11) NOT NULL DEFAULT '0' COMMENT '点赞次数',
  `comment_num` int(11) NOT NULL DEFAULT '0' COMMENT '评论数量',
  `pv` int(11) NOT NULL DEFAULT '0' COMMENT '阅读量',
  `last_comment_at` timestamp NULL DEFAULT NULL COMMENT '最后评论回复时间',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of topics
-- ----------------------------
INSERT INTO `topics` VALUES ('1', 'jobs', '111', '<p>111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 15:24:16', '2018-04-15 15:24:16');
INSERT INTO `topics` VALUES ('2', 'qa', '111', '<p>11111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:04:43', '2018-04-15 18:04:43');
INSERT INTO `topics` VALUES ('3', 'jobs', '111', '<p>11</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:05:40', '2018-04-15 18:05:40');
INSERT INTO `topics` VALUES ('4', 'jobs', '111', '<p>11</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:06:14', '2018-04-15 18:06:14');
INSERT INTO `topics` VALUES ('5', 'jobs', '111', '<p>11</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:08:36', '2018-04-15 18:08:36');
INSERT INTO `topics` VALUES ('6', 'jobs', '111', '<p>11</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:08:45', '2018-04-15 18:08:45');
INSERT INTO `topics` VALUES ('7', 'jobs', '111', '<p>111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:37:47', '2018-04-15 18:37:47');
INSERT INTO `topics` VALUES ('8', 'jobs', '111', '<p>111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:37:58', '2018-04-15 18:37:58');
INSERT INTO `topics` VALUES ('9', 'jobs', '111', '<p>111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:39:04', '2018-04-15 18:39:04');
INSERT INTO `topics` VALUES ('10', 'jobs', '111', '<p>111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:39:13', '2018-04-15 18:39:13');
INSERT INTO `topics` VALUES ('11', 'jobs', '1111', '<p>11111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:43:48', '2018-04-15 18:43:48');
INSERT INTO `topics` VALUES ('12', 'jobs', '1111', '<p>11111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:43:54', '2018-04-15 18:43:54');
INSERT INTO `topics` VALUES ('13', 'jobs', '1111', '<p>11111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:44:02', '2018-04-15 18:44:02');
INSERT INTO `topics` VALUES ('14', 'jobs', '222', '<p>2222</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:47:06', '2018-04-15 18:47:06');
INSERT INTO `topics` VALUES ('15', 'jobs', '222', '<p>2222</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:47:28', '2018-04-15 18:47:28');
INSERT INTO `topics` VALUES ('16', 'jobs', '222', '<p>2222</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:49:54', '2018-04-15 18:49:54');
INSERT INTO `topics` VALUES ('17', 'jobs', '222', '<p>2222</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:51:30', '2018-04-15 18:51:30');
INSERT INTO `topics` VALUES ('18', 'qa', '111', '<p>11111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:54:54', '2018-04-15 18:54:54');
INSERT INTO `topics` VALUES ('19', 'jobs', '111', '<p>1111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:58:28', '2018-04-15 18:58:28');
INSERT INTO `topics` VALUES ('20', 'jobs', '111', '<p>1111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 18:59:42', '2018-04-15 18:59:42');
INSERT INTO `topics` VALUES ('21', 'jobs', '111', '<p>1111</p>\n', '0', '0', '0', '0', '0000-00-00 00:00:00', '2018-04-15 19:00:36', '2018-04-15 19:00:36');
INSERT INTO `topics` VALUES ('22', 'jobs', '111', '<p>1111</p>\n', '0', '0', '0', '0', '2018-04-15 19:01:43', '2018-04-15 19:01:43', '2018-04-15 19:01:43');
INSERT INTO `topics` VALUES ('23', 'jobs', '111', '<p>111</p>\n', '0', '0', '0', '0', '2018-04-15 19:45:04', '2018-04-15 19:45:04', '2018-04-15 19:45:04');
INSERT INTO `topics` VALUES ('24', 'jobs', '111', '<p>111<img src=\"qqq\" alt=\"\"></p>\n', '0', '0', '0', '0', '2018-04-15 19:49:48', '2018-04-15 19:49:48', '2018-04-15 19:49:48');
INSERT INTO `topics` VALUES ('25', 'jobs', '2111', '<p>111</p>\n', '0', '0', '0', '0', '2018-04-15 20:32:38', '2018-04-15 20:32:38', '2018-04-15 20:32:38');
INSERT INTO `topics` VALUES ('26', 'jobs', '111', '<p><img src=\"/upload/59061919-f808-4e4b-a9fc-25ee372029d1.jpg\" alt=\"\"></p>\n', '0', '0', '0', '0', '2018-04-15 20:52:56', '2018-04-15 20:52:56', '2018-04-15 20:52:56');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '昵称',
  `remember_token` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户是否激活',
  `roles` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '身份',
  `gender` varchar(6) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'male' COMMENT '性别。男性：male；女性:female',
  `github_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT 'github名称',
  `real_name` varchar(25) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '真是姓名',
  `city` varchar(25) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '所在城市',
  `company` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '所在公司名称',
  `weibo_name` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '微博名称',
  `weibo_link` varchar(65) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '微博地址',
  `wechat_qrcode_text` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '微信二维码图片地址',
  `payment_qrcode_text` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '支付二维码图片地址',
  `personal_website` varchar(65) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '个人站点',
  `introduction` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '个人简介',
  `signature` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '个人署名',
  `avatar` varchar(65) CHARACTER SET utf8mb4 NOT NULL DEFAULT '/dist/img/icon.png' COMMENT '用户头像地址',
  `email_notify` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否启用邮件通知，布尔值。',
  `like_notify` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启点赞通知，布尔值',
  `jwt_signature` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT 'jwt签名',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', '250476613@qq.com', '$2a$10$ZYldG1zvvvwuiFjxeWeQyu5xRFZwapPhCyjY7MBdgefB8CyLw32AC', 'jobs', '', '0', 'ADMIN', 'male', '', '', '', '', '', '', '', '', '', '', '', '/dist/img/icon.png', '1', '1', '92a77407-bf4b-484e-9792-484dd20498ad', '2018-04-15 20:53:53', '2018-04-15 20:53:53');
