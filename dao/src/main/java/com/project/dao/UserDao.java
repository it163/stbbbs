package com.project.dao;

import com.project.dao.base.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.project.model.User;

@Repository
public interface UserDao extends BaseDao {

    int deleteByPrimaryKey(Integer id);

    int insertUser(User record);

    int insertUserSelective(User record);

    User selectUserByPrimaryKey(Integer id);

    User selectUserByEmail(String email);

    User selectUserByRememberToken(String rememberToken);

    int updateUserByPrimaryKeySelective(User record);

    int updateUserByEmailSelective(User user);

    int deleteUserJwtSignature(int id);

    List<User> selectAllUser();
}
