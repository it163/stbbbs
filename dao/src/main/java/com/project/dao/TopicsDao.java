package com.project.dao;

import com.project.dao.base.BaseDao;
import com.project.model.Topics;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TopicsDao extends BaseDao {

    Topics selectTopicByPrimaryKey(int id);

    List<Topics> selectAllTopics();

    List<Topics> selectTopicsByCategory(String category);

    int deleteTopicByPrimaryKey(int id);

    int createTopic(Topics topics);

    int updateTopicByPrimaryKey(int id);

    Float countTopics();
}
