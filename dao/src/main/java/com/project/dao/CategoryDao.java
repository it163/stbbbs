package com.project.dao;

import com.project.dao.base.BaseDao;
import com.project.model.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryDao extends BaseDao {

    int deleteCategoryByPrimaryKey(Integer id);

    int insertCategory(Category record);

    Category selectCategoryByPrimaryKey(Integer id);

    Category selectCategoryByVar(String var);

    int countCategoryByVarExcludeSelf(Category category);

    List<Category> selectAllCategory();

    int updateCategoryByPrimaryKey(Category record);

    int updateCategoryByVar(Category record);


}
