package com.project.model;

import com.project.util.validation.Exist;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;


public class User {
    private Integer id;

    @NotEmpty(message = "邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 255, message = "邮箱长度不能大于255")
    private String email;

    @NotEmpty(message = "昵称不能为空")
    @Size(min = 0, max = 255, message = "昵称长度不能大于255")
    private String nickname;

    @NotEmpty(message = "密码不能为空")
    @Size(min = 0, max = 255, message = "密码长度不能大于255")
    private String password;

    @NotEmpty(message = "请输入确认密码")
    @Size(min = 0, max = 255, message = "确认密码长度不能大于255")
    private String confirmPassword;
//    @Size(min = 0, max = 255, message = "密码长度不能大于255")
    private String rememberToken;
//    @Size(min = 0, max = 25, message = "角色名长度不能大于25个字节")
    private String roles;
//    @Max(value = 2, message = "用户名激活状态的值不能大于2")
    private Integer active;

    private String code;

//    @Size(min = 0, max = 6, message = "性别字符长度不能大于6")
    private String gender;

//    @Size(min = 0, max = 255, message = "github名称长度不能大于255")
    private String githubName;

//    @Size(min = 0, max = 30, message = "真实名称长度不能大于25个字符")
    private String realName;

//    @Size(min = 0, max = 30, message = "城市名称长度不能大于25个字符")
    private String city;

//    @Size(min = 0, max = 30, message = "公司名称长度不能大于30个字符")
    private String company;

//    @Size(min = 0, max = 30, message = "微博名称长度不能大于30个字符")
    private String weiboName;

//    @Size(min = 0, max = 255, message = "个人简介内容长度不能大于255个字符")
    private String introduction;

//    @Size(min = 0, max = 65, message = "微博链接长度不能大于65个字符")
    private String weiboLink;

//    @Size(min = 0, max = 65, message = "个人站点长度不能大于65个字符")
    private String personalWebsite;

//    @Size(min = 0, max = 65, message = "头像链接地址长度不能大于65个字符")
    private String avatar;

//    @Size(min = 0, max = 255, message = "署名内容不能大于65个字符")
    private String signature;

//    @Max(value = 2, message = "邮件通知状态的值不能大于2")
    private Boolean emailNotify;

//    @Max(value = 2, message = "点赞通知状态的值不能大于2")
    private Boolean likeNotify;

//    @Size(min = 0, max = 100, message = "微信二维码链接长度不能大于100个字符")
    private String wechatQrcodeText;

//    @Size(min = 0, max = 100, message = "支付宝二维码链接长度不能大于100个字符")
    private String paymentQrcodeText;

    private String jwtSignature;

    private String createdAt;

    private String updatedAt;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGithubName() {
        return githubName;
    }

    public void setGithubName(String githubName) {
        this.githubName = githubName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getWeiboName() {
        return weiboName;
    }

    public void setWeiboName(String weiboName) {
        this.weiboName = weiboName;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getWeiboLink() {
        return weiboLink;
    }

    public void setWeiboLink(String weiboLink) {
        this.weiboLink = weiboLink;
    }

    public String getPersonalWebsite() {
        return personalWebsite;
    }

    public void setPersonalWebsite(String personalWebsite) {
        this.personalWebsite = personalWebsite;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getEmailNotify() {
        return emailNotify;
    }

    public void setEmailNotify(Boolean emailNotify) {
        this.emailNotify = emailNotify;
    }

    public Boolean getLikeNotify() {
        return likeNotify;
    }

    public void setLikeNotify(Boolean likeNotify) {
        this.likeNotify = likeNotify;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getWechatQrcodeText() {
        return wechatQrcodeText;
    }

    public void setWechatQrcodeText(String wechatQrcodeText) {
        this.wechatQrcodeText = wechatQrcodeText;
    }

    public String getPaymentQrcodeText() {
        return paymentQrcodeText;
    }

    public void setPaymentQrcodeText(String paymentQrcodeText) {
        this.paymentQrcodeText = paymentQrcodeText;
    }

    public String getJwtSignature() {
        return jwtSignature;
    }

    public void setJwtSignature(String jwtSignature) {
        this.jwtSignature = jwtSignature;
    }
}
