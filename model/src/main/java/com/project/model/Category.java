package com.project.model;

import javax.validation.constraints.Size;

public class Category {

    private Integer id;
    @Size(min = 0, max = 15, message = "栏目常量名称长度不能大于15")
    private String var;
    @Size(min = 0, max = 4, message = "栏目标题长度不能大于15")
    private String title;

    private Boolean status;

    private int sort;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
