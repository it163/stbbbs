package com.project.util.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = ExistValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Exist {
    String message() default "字段已存在";

    String sqlId();


    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
