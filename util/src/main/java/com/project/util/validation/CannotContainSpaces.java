package com.project.util.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = CannotContainSpacesValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CannotContainSpaces {
    String message() default "{Cannot.contain.Spaces}";

    int length() default 5;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
