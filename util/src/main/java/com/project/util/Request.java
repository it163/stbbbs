package com.project.util;

import javax.servlet.http.HttpServletRequest;

public class Request {
    /**
     * 判断请求是否阿伟ajax
     * @param request
     * @return boolean
     */
    public static Boolean isAjax(HttpServletRequest request) {
        String requestType = request.getHeader("X-Requested-With");
        if (requestType == null) {
            return false;
        }
        if (requestType.equals("XMLHttpRequest")) {
            return true;
        }
        return false;
    }
}
