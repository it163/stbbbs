package com.project.util;

public class View {
    /**
     * 判断页面当前的uri是否与给定的模式匹配
     * @param currentUri
     * @param pattern
     * @param active
     * @return
     */
    public String active(String currentUri,String pattern,String active) {
        Boolean math = Url.match(pattern,currentUri);
        if (math) {
            return active;
        } else {
            return "";
        }
    }
    public String active(String currentUri,String pattern) {
        Boolean math = Url.match(pattern,currentUri);
        if (math) {
            return "active";
        } else {
            return "";
        }
    }
}
