package com.project.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Url {
    /**
     * 通过给定的url表达式，判断被比较的内容是否匹配
     * @param path
     * @param content
     * @return
     */
    public static boolean match(String path, String content) {
        String[] uris = path.split(",");
        List<String> newUris = new ArrayList<>();
        for (String uri : uris) {
            String newUri = uri.replaceFirst("\\*", ".*");
            newUris.add(newUri);
            if (Pattern.matches(".*/\\*",uri)) {
                String newUri2 = uri.replaceFirst("/\\*","");
                newUris.add(newUri2);
            }
        }
        String str1 = Arrays.toString(newUris.toArray()).replaceAll(",","|").replaceAll(" ","");
        String newPattern = str1.substring(1,str1.length()-1);
        boolean isMatch = Pattern.matches(newPattern,content);
        return isMatch;
    }
}
