const util = {
    validation: {
        checkEmail: function (email) {
            var myReg = /^\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}$/;
            if (myReg.test(email)) {
                return true;
            } else {
                return false;
            }
        }
    },
    uploadSingle: function (app,form,_this,maxSie) {

        $(form).ajaxSubmit({
            dataType:'json',
            url:'/upload/single',
            data:{
                "field":$(_this).attr('name'),
                "maxSize":maxSie * 1024 * 1024
            },
            success:function(response) {
                if (response.status) {
                    var fieldText = $(_this).next('input').attr('name');
                    app.form.field[fieldText] = response.content
                    if ($(_this).next().next('img').length > 0) {
                        if ($(_this).next().next().next('span.clear-image').length > 0) {
                            $(_this).next().next().next('span.clear-image').remove();
                        }
                        $(_this).next().next('img').attr("src",response.content);

                        $(_this).next().next('img').attr("width",358);
                        $(_this).next().next('img').attr("height",329);
                    }
                } else {
                    alert(response.content)
                }
            },
            error:function (response) {
                alert('网络异常，或者是上传的文件太大超过服务器负载(服务器拒绝大小超过100M的文件)，请稍后或者换小点的文件再试')
            }
        });
    },
    delAttachment:function(app,_this,realDel = false) {
        var path = $(_this).prev('img').attr('src');
        var field = $(_this).parent().find('.upload-hidden').attr('name');
        $(_this).prev('img').attr('src','');
        $(_this).prev('img').removeAttr('width');
        $(_this).prev('img').removeAttr('height');
        app.form.field[field] = '';
        $(_this).remove();
        if (realDel) {
            $.post('/upload/remove',{path:path},function(){

            });
        }
    }
};