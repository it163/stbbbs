package com.project.controller;//package com.jobs.controller;


import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping(value = "/upload")
public class UploadController {

    @Value("${app.upload-path}")
    String uploadPath;

    /**
     * 上传单个文件
     *
     * @param request
     * @return
     */
    @ResponseBody
    @PostMapping("/single")
    public String single(MultipartHttpServletRequest request) {
        String field = request.getParameter("field");
        String maxSizeStr = request.getParameter("maxSize");
        long maxSize = 10 * 1024 * 1024;
        if (maxSizeStr != null) {
            maxSize = Integer.parseInt(maxSizeStr);
        }
        Map<String, Object> responseStr = new HashMap<>();
        try {
            File classPathFile = new File(uploadPath);
            MultipartFile file = request.getFile(field);
            File uploadDir = new File(classPathFile.getAbsolutePath(), "upload/");
            if (!uploadDir.exists()) {
                uploadDir.mkdirs();
            }
            if (file.getSize() > maxSize) {
                responseStr.put("status", false);
                responseStr.put("content", "上传文件不能大于" + maxSize / 1024 / 1024 + "M");
                return JSON.toJSONString(responseStr);
            }
            String filename = file.getOriginalFilename();
            String suffix = filename.substring(filename.lastIndexOf(".") + 1);
            filename = UUID.randomUUID().toString() + "." + suffix;
            File serverFile = new File(uploadDir.getAbsolutePath() + File.separator + filename);
            file.transferTo(serverFile);
            responseStr.put("status", true);
            responseStr.put("content", "/upload/" + filename);
        } catch (Exception e) {
            responseStr.put("status", false);
            responseStr.put("content", e.getMessage());
        }
        return JSON.toJSONString(responseStr);
    }

    @ResponseBody
    @PostMapping("/markdown")
    public String markdown(MultipartHttpServletRequest request) {
        String field = "editormd-image-file";
        String maxSizeStr = request.getParameter("maxSize");
        long maxSize = 10 * 1024 * 1024;
        if (maxSizeStr != null) {
            maxSize = Integer.parseInt(maxSizeStr);
        }
        Map<String, Object> responseStr = new HashMap<>();
        try {
            File classPathFile = new File(uploadPath);
            MultipartFile file = request.getFile(field);
            File uploadDir = new File(classPathFile.getAbsolutePath(), "upload/");
            if (!uploadDir.exists()) {
                uploadDir.mkdirs();
            }
            if (file.getSize() > maxSize) {
                responseStr.put("success", 0);
                responseStr.put("message", "上传文件不能大于" + maxSize / 1024 / 1024 + "M");
                return JSON.toJSONString(responseStr);
            }
            String filename = file.getOriginalFilename();
            String suffix = filename.substring(filename.lastIndexOf(".") + 1);
            filename = UUID.randomUUID().toString() + "." + suffix;
            File serverFile = new File(uploadDir.getAbsolutePath() + File.separator + filename);
            file.transferTo(serverFile);
            responseStr.put("success", 1);
            responseStr.put("message", "上传成功");
            responseStr.put("url", "/upload/" + filename);
        } catch (Exception e) {
            responseStr.put("success", 0);
            responseStr.put("message", e.getMessage());
        }
        System.out.println(JSON.toJSONString(responseStr));
        return JSON.toJSONString(responseStr);
    }

    /**
     * 删除上传的图片
     *
     * @param request
     * @return
     */
    @ResponseBody
    @PostMapping("/remove")
    public String remove(HttpServletRequest request) {
        Map<String, Object> responseStr = new HashMap<>();
        responseStr.put("status", false);
        responseStr.put("content", "没有删除任何东西");
        String uriPath = request.getParameter("path");
        if (uriPath != null) {
            try {
                File classPathFile = new File(ResourceUtils.getURL("classpath:").getPath());
                File file = new File(classPathFile.getPath(), "static" + File.separator + uriPath);
                if (file.exists() && file.isFile()) {
                    file.delete();
                    responseStr.put("status", true);
                    responseStr.put("content", "删除成功");
                }
            } catch (Exception e) {

            }

        }
        return JSON.toJSONString(responseStr);
    }
}
