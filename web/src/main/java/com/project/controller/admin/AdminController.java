package com.project.controller.admin;

import com.alibaba.fastjson.JSON;
import com.project.model.Category;
import com.project.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("admin")
public class AdminController extends BaseController {
    @Autowired
    CategoryService categoryService;
    @GetMapping("")
    public String index() {
        return "admin/index";
    }

    @GetMapping("category")
    public String category() {
        return "admin/category";
    }

    @ResponseBody
    @PostMapping("saveCategory")
    public String saveCategory(
            @Valid Category category,
            BindingResult result,
            HttpServletRequest request
    ) {
        Map<String, Object> map = new HashMap<>();
        if (result.hasErrors()) {
            map.put("status",false);
            map.put("msg",result.getAllErrors().get(0).getDefaultMessage());
            return JSON.toJSONString(map);
        }
        String id = request.getParameter("id");
        if (id == null) {
            Category record = categoryService.selectCategoryByVar(category.getVar());
            if (record != null) {
                map.put("varStatus", false);
                map.put("msg", "该栏目已经存在了，请换一个常量名称");
                return JSON.toJSONString(map);
            }
            if (categoryService.insertCategory(category) > 0) {
                map.put("status",true);
                map.put("msg","添加栏目成功");
                return JSON.toJSONString(map);
            } else {
                map.put("status",false);
                map.put("msg","网络异常，请稍后重新再试");
                return JSON.toJSONString(map);
            }
        } else {
            if (categoryService.countByVarExcludeSelf(category) > 0) {
                map.put("varStatus", false);
                map.put("msg", "你使用的常量已经存在了");
                return JSON.toJSONString(map);
            }
            if (categoryService.updateCategory(category) > 0) {
                map.put("status",true);
                map.put("msg","修改栏目成功");
                return JSON.toJSONString(map);
            } else {
                map.put("status",false);
                map.put("msg","网络异常，请稍后重新再试");
                return JSON.toJSONString(map);
            }
        }

    }
    @ResponseBody
    @PostMapping("getCategory")
    public String getCategory(HttpServletRequest request) {
        Integer id = Integer.parseInt(request.getParameter("id"));
        Category category = categoryService.selectCategoryByPk(id);
        return JSON.toJSONString(category);
    }
    @ResponseBody
    @PostMapping("getCategoryList")
    public String getCategoryList() {
        List<Category> categoryList = categoryService.selectAllCategory();
        return JSON.toJSONString(categoryList);
    }

    /**
     * 删除一条栏目
     * @param request
     * @return
     */
    @ResponseBody
    @PostMapping("deleteCategory")
    public String deleteCategory(HttpServletRequest request) {
        Integer id = Integer.parseInt(request.getParameter("id")) ;
        Category record = categoryService.selectCategoryByPk(id);
        Map<String,Object> map = new HashMap<>();
        if (record == null) {
            map.put("status",false);
            map.put("msg","记录不存在");
            return JSON.toJSONString(map);
        }
        int res = categoryService.deleteCategoryByPrimaryKey(id);
        if (res > 0) {
            map.put("status",true);
            map.put("msg","删除成功");
        } else {
            map.put("status",false);
            map.put("msg","网络异常，请稍后再试");
        }
        return JSON.toJSONString(map);
    }
}
