package com.project.controller.admin;

import org.springframework.web.bind.annotation.ModelAttribute;

public class BaseController extends com.project.controller.base.BaseController {
    @ModelAttribute
    public void checkAuth() {

    }
}
