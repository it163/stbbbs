package com.project.controller.home;

import com.alibaba.fastjson.JSON;
import com.project.controller.base.BaseController;
import com.project.model.User;
import com.project.service.UserService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;

    /**
     * 编辑个人资料主页
     *
     * @return
     */
    @GetMapping("")
    public String index() {
        return "home/user/index";
    }

    /**
     * 获取用户数据
     *
     * @param request
     * @return
     */
    @ResponseBody
    @PostMapping("/getData")
    public String getData(HttpServletRequest request) {
        User user = userService.selectByPk(userService.getCurrentUser(request).getId());
        return JSON.toJSONString(userService.getUserEditInfoByUser(user));
    }

    /**
     * 修改用户信息
     *
     * @param parameter
     * @param request
     * @return
     */
    @ResponseBody
    @PostMapping("/edit")
    public String edit(User parameter, HttpServletRequest request) {
        Map<String, Object> responseMap = new HashMap<>();
        User currentUser = userService.getCurrentUser(request);
        if (currentUser.getId() != parameter.getId()) {
            responseMap.put("status", false);
            responseMap.put("content", "您修改的不是自己的用户信息");
        }
        if (parameter.getPassword() != null) {
            parameter.setPassword(BCrypt.hashpw(parameter.getPassword(), BCrypt.gensalt()));
        }
        int res = userService.updateUser(parameter);
        if (res > 0) {
            User newUser = userService.selectByEmail(currentUser.getEmail());
            request.getSession().setAttribute("user",newUser);
            responseMap.put("status", true);
        } else {
            responseMap.put("status", false);
            responseMap.put("content", "系统异常，请善后再试");
        }

        return JSON.toJSONString(responseMap);
    }

    /**
     * 修改用户头像主页
     *
     * @return
     */
    @GetMapping("editAvatar")
    public String editAvatar() {
        return "home/user/editAvatar";
    }

    /**
     * 通知设置主页
     *
     * @return
     */
    @GetMapping("editEmailNotify")
    public String editEmailNotify() {
        return "home/user/editEmailNotify";
    }

    /**
     * 修改用户密码
     *
     * @return
     */
    @GetMapping("editPassword")
    public String editPassword() {
        return "home/user/editPassword";
    }

    @GetMapping("profile")
    public String profile() {
        return "home/user/profile";
    }
}
