package com.project.service;

import com.project.model.Category;

import java.util.List;
import java.util.Map;

public interface CategoryService {
    int insertCategory(Category record);

    List<Category> pageCategory(int pageNum, int pageSize);

    List<Category> selectAllCategory();

    Category selectCategoryByVar(String var);

    Category selectCategoryByPk(int id);

    int countByVarExcludeSelf(Category category);

    int updateCategory(Category record);

    int deleteCategoryByPrimaryKey(Integer id);

    void setError(String error);

    String getError();
}
