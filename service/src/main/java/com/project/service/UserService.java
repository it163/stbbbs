package com.project.service;

import java.util.List;
import java.util.Map;

import com.project.model.User;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    int addUser(User user);

    List<User> findAllUser(int pageNum, int pageSize);

    User selectByEmail(String username);

    User selectByRememberToken(String rememberToken);

    User selectByPk(int id);

    User getCurrentUser(HttpServletRequest request);

    Map getUserEditInfoByUser(User user);

    int setUserRememberToken(User user);

    int updateUser(User user);

    int deleteJwtSignature(int id);

    Integer getUserIdFromAccessToken(String token);

    String getCurrentJwtSignatureByPk(int id);

    /**
     * 判断用户是不是后台管理员用户
     *
     * @param user
     * @return
     */
    Boolean isAdmin(User user);

    /**
     * 判断是否登录
     *
     * @param request
     * @return
     */
    Boolean hasLogin(HttpServletRequest request);

    void setError(String error);

    String getError();
}
