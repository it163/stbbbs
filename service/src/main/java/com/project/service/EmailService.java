package com.project.service;

public interface EmailService {
    void send(String toEmail, String subject,String text);
}
