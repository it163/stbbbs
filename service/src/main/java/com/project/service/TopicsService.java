package com.project.service;

import com.project.model.Category;
import com.project.model.Topics;

import java.util.List;

public interface TopicsService {
    Topics selectByPrimaryKey(int id);

    List<Topics> selectAll(int page,int size);

    List<Topics> selectTopicsByCategory(String category);

    String getEditInfo(int id);

    int deleteByPrimaryKey(int id);

    int create(Topics topics);

    int updateByPrimaryKey(int id);

    float count();

    void setError(String error);

    String getError();
}
