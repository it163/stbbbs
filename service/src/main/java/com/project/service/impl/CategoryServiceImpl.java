package com.project.service.impl;

import com.github.pagehelper.PageHelper;
import com.project.dao.CategoryDao;
import com.project.model.Category;
import com.project.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "CategoryService")
public class CategoryServiceImpl implements CategoryService {
    private String error;
    @Autowired
    CategoryDao categoryDao;

    @Override
    public int insertCategory(Category record) {
        return categoryDao.insertCategory(record);
    }

    @Override
    public List<Category> pageCategory(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return categoryDao.selectAllCategory();
    }

    @Override
    public Category selectCategoryByVar(String var) {
        return categoryDao.selectCategoryByVar(var);
    }

    @Override
    public Category selectCategoryByPk(int id) {
        return categoryDao.selectCategoryByPrimaryKey(id);
    }

    @Override
    public int countByVarExcludeSelf(Category category) {
        return categoryDao.countCategoryByVarExcludeSelf(category);
    }

    @Override
    public List<Category> selectAllCategory() {
        return categoryDao.selectAllCategory();
    }

    @Override
    public int updateCategory(Category record) {
        return categoryDao.updateCategoryByPrimaryKey(record);
    }

    @Override
    public int deleteCategoryByPrimaryKey(Integer id) {
        return categoryDao.deleteCategoryByPrimaryKey(id);
    }

    @Override
    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String getError() {
        return error;
    }
}
