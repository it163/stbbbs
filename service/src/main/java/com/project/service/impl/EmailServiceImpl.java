package com.project.service.impl;


import com.project.service.EmailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;


@Service(value = "EmailService")
public class EmailServiceImpl implements EmailService {
    private String error;
    @Autowired
    JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    String fromEmail;

    @Value("${spring.mail.personal}")
    String personal;

    public void send(String toEmail, String subject, String text) {
        try {
            final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setFrom(fromEmail, personal);
            message.setTo(toEmail);
            message.setSubject(subject);
            message.setText(text);
            this.mailSender.send(mimeMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
